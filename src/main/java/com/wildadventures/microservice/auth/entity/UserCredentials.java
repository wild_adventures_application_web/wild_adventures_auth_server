package com.wildadventures.microservice.auth.entity;

import lombok.Data;

@Data
public class UserCredentials {
    private String username;
    private String password;
}
