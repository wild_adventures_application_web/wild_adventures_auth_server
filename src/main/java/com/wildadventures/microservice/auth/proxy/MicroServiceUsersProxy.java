package com.wildadventures.microservice.auth.proxy;

import com.wildadventures.microservice.auth.bean.UserDetailsG;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;


/**
 * Proxy MicroService Feign and Ribbon
 * @author amarjane
 * @version 1.0
 */
@FeignClient("microservice-users")
@RequestMapping("${microservice-users.context-path}")
public interface MicroServiceUsersProxy {


    /**
     * Search User with mail type String
     * @link
     * @param mail
     * @return ResponseEntity<?>
     */
    @GetMapping(value = "/users/mail/{mail}")
    ResponseEntity<UserDetailsG> readOneUsersByMail(@PathVariable("mail") String mail);


}
