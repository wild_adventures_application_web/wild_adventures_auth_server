package com.wildadventures.microservice.auth.security;

import com.wildadventures.microservice.auth.bean.UserDetailsG;
import com.wildadventures.microservice.auth.proxy.MicroServiceUsersProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private MicroServiceUsersProxy microServiceUsersProxy;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetailsG userAccount = getUserByMail(username);
        if (userAccount == null) throw new UsernameNotFoundException("Username: " + username + " not found");
        else {
            // Remember that Spring needs roles to be in this format: "ROLE_" + userRole (i.e. "ROLE_ADMIN")
            // So, we need to set it to that format, so we can verify and compare roles (i.e. hasRole("ADMIN")).
            List<GrantedAuthority> grantedAuthorities =
                    AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_" + userAccount.getProfil());

            // The "User" class is provided by Spring and represents a model class for user to be returned by UserDetailsService
            // And used by auth manager to verify and check user authentication.
            return new User(userAccount.getMail(), userAccount.getPassword(), grantedAuthorities);
        }
    }

    private UserDetailsG getUserByMail(String mail){
        ResponseEntity<UserDetailsG> entity = microServiceUsersProxy.readOneUsersByMail(mail);
        return entity.getBody();
    }
}
