package com.wildadventures.microservice.auth.bean;

public enum ProfilEnum {
    CLIENT("CLIENT"),
    ADMIN("ADMIN");

    private final String message;

    /**
     * @param message
     */
    ProfilEnum(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
